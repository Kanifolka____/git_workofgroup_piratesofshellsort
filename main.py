import os


def read_file():
    """ Функция для чтения файла
       Выводит в консоль прочитанную строку
       Возвращает значение прочитанной строки
    """
    file = 'file.txt'
    with open(file, 'r', encoding='utf-8') as f:
        data = f.read()
        print(data)
        return data



def write_file():
    """ Функция для создания файла
        И заполнения базовым сообщением
    """
    with open('file.txt', 'w') as writer:
        writer.write('I love Git, but i hate create SSH')
        return True



def delete_file():
    """Функция для удаления файла
        Способна возвращать как результат TRUE / FALSE
    """
    file_path = 'file.txt'
    try:
        os.remove(file_path)
        return True
    except:
        print("The system can not find the file specified")
        return False


if __name__ == "__main__":
    if write_file(): # Создаем и пишем в файл
        print("[OK] write")

    print(read_file()) # читаем содержимое файла

    if delete_file(): #удаляем файл
        print("[OK] delete")

